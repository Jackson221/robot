# About

This was a fork of my game-engine, later trimmed down to be mostly standalone with some Module3D modules used.

Its purpose is to control a Raspberry Pi based robot with camera feedback.

# Building & running

You may wish to consider utilizing sshfs to store the project files on the raspberry pi itself.

**(Server)** `mkdir build_server && cd build_server && cmake .. -DSERVER=on`

You may wish to cross-compile. Instructions for doing so are coming soon.

**(Client)** `mkdir build_client && cd build_client && cmake ..`

**Both**

make -j5

## Server Deps:

* ImageMagick++
* wiringPi

## Client Deps:

* SDL2
* SDL2_TTF
* SDL_Image
