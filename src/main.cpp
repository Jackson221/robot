/*
Copyright 2017-2020 Jackson Reed McNeil

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef SERVER
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#endif


#include <stdio.h>
#include <pthread.h>
#include <math.h>

#include <memory>
#include <vector>
#include <functional>
#include <string>
#include <iostream>
#include <cmath>

#include "serialize.h"
#include "network.h"

#include "synchro/synchro.hpp"

#ifdef SERVER
	#include <raspicam/raspicam.h>
	#include <Magick++.h>
	#include "robot-control.h"
#endif

#ifndef SERVER
//stolen from a legacy version of my game engine
int img_to_texture(SDL_Texture** result, SDL_Renderer* renderer, SDL_RWops* data)
{
	SDL_Surface* LoadingSurface;
	LoadingSurface = IMG_Load_RW(data,1);
	if (LoadingSurface == NULL)
	{
		SDL_FreeSurface(LoadingSurface);
		return 1; 
	}
	*result = SDL_CreateTextureFromSurface(renderer, LoadingSurface);
	SDL_FreeSurface(LoadingSurface);
	if (result == NULL)
	{
		return 2;
	}
	return 0;
}
SDL_Renderer* renderer;
SDL_Window* window;

#endif

GE_NetworkSocket* mySocket;



enum event_types
{
	NONE,
	MOVING,
    ISO,
	SIZE,
};
struct moving_event
{
	event_types type = MOVING;
	/*!
	 * Which side's wheels
	 */
	enum direction_t
	{
		LEFT,
		RIGHT,
	};
	direction_t direction;
	signed char intensity;

	moving_event()
	{
	}
	moving_event(direction_t direction, signed char intensity)
	{
		this->direction = direction;
		this->intensity = intensity;
	}



	GE_FORCE_INLINE constexpr static void serialize(moving_event* serializeMe, serialization::serialization_state& state)
	{
		serialization::serialize(static_cast<int>(serializeMe->direction),state);
		serialization::serialize(serializeMe->intensity,state);
	}
	static moving_event* unserialize(serialization::unserialization_state& state)
	{
		auto direction = static_cast<direction_t>(serialization::unserialize<int>(state));
		auto intensity = serialization::unserialize<signed char>(state);
		return new moving_event(direction,intensity);
	}
};
struct iso_event
{
    event_types type = ISO;
    int set_to = 66666;
    
    iso_event();
    iso_event(int iso) : set_to(iso) {}
    GE_FORCE_INLINE constexpr static void serialize(iso_event* serializeMe, serialization::serialization_state& state)
	{
		serialization::serialize(static_cast<int>(serializeMe->set_to),state);
	}
	static iso_event* unserialize(serialization::unserialization_state& state)
	{
		auto iso = static_cast<int>(serialization::unserialize<int>(state));
		return new iso_event(iso);
	}
};


/*!
 * Size SHALL NOT exceed 8 bytes, including the type of the data.
 */
union robot_event
{
	robot_event(){}
	event_types type;
	moving_event moving;
    iso_event iso;
	GE_FORCE_INLINE constexpr static void serialize(robot_event* serializeMe, serialization::serialization_state& state)
	{
		serialization::serialize(static_cast<int>(serializeMe->type),state); 
		if (serializeMe->type == MOVING)
		{
			serialization::serialize(&serializeMe->moving,state);
		}
		else if (serializeMe->type == ISO)
        {
            serialization::serialize(static_cast<int>(serializeMe->iso.set_to),state);
        }
	}
	static robot_event* unserialize(serialization::unserialization_state& state)
	{
		auto returnval = new robot_event{};
		auto type = static_cast<event_types>(serialization::unserialize<int>(state));
		returnval->type = type;
		if (type == MOVING)
		{
			returnval->moving = serialization::unserialize<moving_event>(state);
		}
		else if (type == ISO)
        {
            returnval->iso = serialization::unserialize<iso_event>(state);
        }
		return returnval;
	}
};

std::vector<robot_event> all_events;
int global_iso = 0;


#define set_both_sides(left,right) \
auto newevent = robot_event{};\
newevent.moving = moving_event(moving_event::LEFT,left);\
all_events.push_back(newevent);\
newevent = robot_event{};\
newevent.moving = moving_event(moving_event::RIGHT,right);\
all_events.push_back(newevent);


#ifndef SERVER
class Background
{
	public:
		Background(SDL_Renderer* renderer)
		{
			this->renderer = renderer;
		}
		~Background()
		{
			SDL_DestroyTexture(currentImage);
		}
		void render()
		{
			if (currentImage != NULL)
			{
				SDL_Rect position =  {0,0,640,480};
				SDL_RenderCopy(renderer,currentImage,NULL,&position);
			}
		}
		void give_event(SDL_Event event)
		{
			if(event.type == SDL_KEYDOWN || event.type == SDL_KEYUP)
			{
				if (event.key.keysym.sym == SDLK_w)
				{
					held[w] = event.type == SDL_KEYDOWN;	
				}
				else if (event.key.keysym.sym == SDLK_s)
				{
					held[s] = event.type == SDL_KEYDOWN;	
				}
				else if (event.key.keysym.sym == SDLK_d)
				{
					held[d] = event.type == SDL_KEYDOWN;	
				}
				else if (event.key.keysym.sym == SDLK_a)
				{
					held[a] = event.type == SDL_KEYDOWN;	
				}
				if (event.type == SDL_KEYUP)
                {
                    bool send = true;
                    if (event.key.keysym.sym == SDLK_UP)
                    {
                        global_iso += 10;
                    }
                    else if (event.key.keysym.sym == SDLK_DOWN)
                    {
                        global_iso -= 10;
                    }
                    else
                    {
                        send = false;
                    }
                    if (send)
                    {
                        global_iso = std::clamp(global_iso, 0, 1000);
                        auto newevent = robot_event{};
                        newevent.iso = iso_event{global_iso};
                        all_events.push_back(newevent);
                        printf("send iso %d\n", global_iso);
                    }
                }
				/*!
				 * Basically, a & d are considered modifers to w and s -- they are individual commands only if w/s are not held.
				 */
				if (held[w])
				{
					if (held[a])
					{
						set_both_sides(0,127);
					}
					else if (held[d])
					{
						set_both_sides(127,0);
					}
					else
					{
						set_both_sides(127,127);
					}
				}
				else if (held[s])
				{
					if (held[a])
					{
						set_both_sides(-127,0);
					}
					else if (held[d])
					{
						set_both_sides(0,-127);
					}
					else
					{
						set_both_sides(-127,-127);
					}
				}
				else if (held[a])
				{
					set_both_sides(-127,127);
				}
				else if (held[d])
				{
					set_both_sides(127,-127);
				}
				else
				{
					set_both_sides(0,0);
				}
			}
		}
		void set_image(const char* image_data, size_t size)
		{
			SDL_DestroyTexture(currentImage);
			currentImage = NULL;

			SDL_RWops* data_in_rwops = SDL_RWFromConstMem(image_data,size);
			img_to_texture(&currentImage,renderer,data_in_rwops);
		}
	private:
		SDL_Renderer* renderer;

		enum hold_directions
		{
			w,
			s,
			d,
			a,
			SIZE
		};
		bool held[SIZE] = {0,};
		SDL_Texture* currentImage = NULL;
};
#endif

void printallevents()
{
	bool sentanyevents=false;
	for (auto event : all_events)
	{
		sentanyevents=true;
		printf("event type %d direction %d intensity %d\n",event.type,event.moving.direction,event.moving.intensity);
	}
	if (sentanyevents)
	{
		printf("End of events\n");
	}
}


void write_size_of_incoming_message(GE_NetworkSocket* receivingSocket, size_t length)
{
	auto state = serialization::serialization_state(0);

	serialization::serialize(static_cast<serialization::size_tp>(length),state);

	GE_Write(receivingSocket, state.buffer,12); //must write exactly 12 bytes
}
size_t read_size_of_incoming_message(GE_NetworkSocket* mySocket)
{
	char buffer[12] = ""; //Has to contain the version number too
	GE_ReadAll(mySocket,buffer,sizeof(buffer));

	auto state = serialization::unserialization_state(buffer);
	return static_cast<size_t>(serialization::unserialize<serialization::size_tp>(state));
}

#ifdef SERVER

	raspicam::RaspiCam camera;
	Magick::Image capture_ppm()
	{

		
		camera.grab();

		std::vector<unsigned char> buf;
		buf.resize(camera.getImageTypeSize(raspicam::RASPICAM_FORMAT_RGB));

		
		camera.retrieve(buf.data(), raspicam::RASPICAM_FORMAT_IGNORE);


		std::string header = "P6\n" + std::to_string(camera.getWidth()) + " " + std::to_string(camera.getHeight()) + " 255\n";
		buf.insert(buf.begin(),std::begin(header),std::end(header));

		Magick::Blob myblob = Magick::Blob(buf.data(),buf.size());
		
		Magick::Image resultant_image(myblob);
		return resultant_image;
	}
	void resize_image(Magick::Image& image)
	{
		auto new_aspect = Magick::Geometry(640,480);
		image.scale(new_aspect);
	}
	void convert_to_jpeg(Magick::Image& image)
	{
		image.magick("JPEG");	
	}
	Magick::Blob convert_to_blob(Magick::Image& image)
	{
		Magick::Blob converted_blob = Magick::Blob();
		image.write(&converted_blob);
		return converted_blob;
	}

	
	void write_image_to_client(GE_NetworkSocket* clientSocket, Magick::Blob& image_blob)
	{
		GE_Write(clientSocket, static_cast<const char*>(image_blob.data()),image_blob.length());
	}

	

#endif

int main(int argc, char* argv[])
{
	GE_NetworkSocket* mySocket = GE_CreateNetworkSocket();

	GE_FillNetworkSocket(mySocket,5668);
#ifdef SERVER


	
	Init_Robot();

	camera.setCaptureSize(320,240);//640,480);
	camera.setVideoStabilization(true);
	camera.setHorizontalFlip(true);
	camera.setVerticalFlip(true);

	if(!camera.open())
	{
		printf("couldnt open camera\n");
	}
	printf("Server\n");


	
	GE_BindServer(mySocket);
	GE_NetworkSocket* clientSocket = GE_CreateNetworkSocket();
	printf("Connected Client: %d\n",GE_ConnectClient(clientSocket,mySocket));



	while (true)
	{
		/*Magick::Image image = capture_ppm();
		//convert_to_jpeg(image);
		resize_image(image);
		Magick::Blob converted_blob = convert_to_blob(image);*/
		camera.setISO(global_iso);
		camera.grab();

		std::vector<unsigned char> buf;
		buf.resize(camera.getImageTypeSize(raspicam::RASPICAM_FORMAT_RGB));
	
		camera.retrieve(buf.data());


		std::string header = "P6\n" + std::to_string(camera.getWidth()) + " " + std::to_string(camera.getHeight()) + " 255\n";
		buf.insert(buf.begin(),std::begin(header),std::end(header));

		Magick::Blob converted_blob = Magick::Blob(buf.data(),buf.size());


		write_size_of_incoming_message(clientSocket,converted_blob.length());
		write_image_to_client(clientSocket,converted_blob);

		std::vector<char> buffer;
		buffer.resize(read_size_of_incoming_message(clientSocket));

		GE_ReadAll(clientSocket, buffer.data(),buffer.size());
		auto state = serialization::unserialization_state(buffer.data());
		all_events = serialization::unserialize<std::vector<robot_event>>(state);


		for (auto event : all_events)
		{
			if (event.type == MOVING)
			{
				if(event.moving.direction == moving_event::LEFT)
				{
					setLeft(event.moving.intensity);
				}
				else if(event.moving.direction == moving_event::RIGHT)
				{
					setRight(event.moving.intensity);
				}
			}
			else if (event.type == ISO)
            {
                global_iso = event.iso.set_to;
                printf("set iso %d\n", global_iso);
            }
		}
		printallevents();
		all_events.clear();
	}
	/*
	raspicam::RaspiCam camera;

	if(!camera.open())
	{
		printf("couldnt open camera\n");
	}
	camera.grab();

	std::vector<unsigned char> buf;
	buf.resize(camera.getImageTypeSize(raspicam::RASPICAM_FORMAT_RGB));

	camera.retrieve(buf.data(), raspicam::RASPICAM_FORMAT_IGNORE);

	std::ofstream outFile("output.ppm", std::ios::binary);
	outFile << "P6\n" << camera.getWidth() << " " << camera.getHeight() << " 255\n";
	outFile.write((char*)buf.data(), buf.size());
	*/
	GE_FreeNetworkSocket(clientSocket);
	camera.release();
	Shutdown_Robot();

	
#else //CLIENT
	printf("Client\n");
	printf(" cs %d\n",GE_ConnectServer(mySocket,(char*)"127.0.0.1"));

	printf("I'm alive maybe?\n");



	int error = SDL_CreateWindowAndRenderer(640,480,0,&window,&renderer);
	if (error != 0)
	{
		printf("Game engine initialization error: %d\n",error);
		return error;
	}
	SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "0" );
    auto font = TTF_OpenFont("../fonts/gnuFreeFonts/FreeMono.ttf", 12);
    
	auto background = Background(renderer);
    printf("begin\n");
	while (true)
	{
		SDL_Event event;
        while(SDL_PollEvent (&event))
        {
            background.give_event(event);
        }

		//deserialize image
		{
			size_t length = read_size_of_incoming_message(mySocket);
			printf("Length to allocate %zu\n",length);

			std::vector<char> buffer;
			buffer.resize(length);
			printf("Read image of size %zu\n",GE_ReadAll(mySocket,buffer.data(), length));

			background.set_image(buffer.data(),length);
		}


		//serialize inputs
		{
			serialization::serialization_state state = serialization::serialization_state(0);

			serialization::serialize(&all_events,state);

			write_size_of_incoming_message(mySocket,state.bufferUsed);

			GE_Write(mySocket, state.buffer,state.bufferUsed);
		}

		printallevents();
		all_events.clear();
        
        background.render();
		SDL_RenderPresent(renderer); 
	}
	printf("--BEGIN SHUTDOWN--\n");
	SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
	printf("Bye.\n");
    
    TTF_CloseFont(font);


#endif
	

	GE_FreeNetworkSocket(mySocket);
	return 0;
}

