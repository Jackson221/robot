#include "network.h"


#include "GeneralEngineCPP.h"
#ifdef outdatedOS

#define WIN32_LEAN_AND_MEAN 
#include <winsock2.h>
#include <windows.h>

#define read(a,b,c) ReadFile(a,b,c)

#define close(socket) closesocket(socket)

#else

#include <unistd.h>  
#include <netinet/in.h> 
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>


#endif

#include <stdlib.h> 
#include <cstring>   
#include <string>
#include <cstdio>
#include <new>




struct GE_NetworkSocket
{
	int socketfd;
	struct sockaddr_in addr;
};

GE_NetworkSocket* GE_CreateNetworkSocket()
{
	return new GE_NetworkSocket{};
}

int GE_FillNetworkSocket(GE_NetworkSocket* aSocket, unsigned short port)
{
	aSocket->socketfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (aSocket->socketfd < 0 )
	{
		return aSocket->socketfd;
	}
	memset(&(aSocket->addr),0,sizeof(aSocket->addr));

	aSocket->addr.sin_family = AF_INET;
	aSocket->addr.sin_addr.s_addr = INADDR_ANY;
	aSocket->addr.sin_port = htons(port);
	return 0;

}
int GE_BindServer(GE_NetworkSocket* nSocket)
{
	int bindError = bind(nSocket->socketfd, (struct sockaddr *) &nSocket->addr, sizeof(nSocket->addr));

	if (bindError < 0)
	{
		return bindError;
	}

	return 0;
}
int GE_ConnectServer(GE_NetworkSocket* sSocket, char* hostname)
{
	struct hostent *server = gethostbyname(hostname);
	if (server == NULL) 
	{
		return 1;
	}
	memmove((void *)server->h_addr, (void *)&(sSocket->addr).sin_addr.s_addr, server->h_length);
	int connectError = connect(sSocket->socketfd, (struct sockaddr*)&sSocket->addr, sizeof(sSocket->addr));
	if (connectError < 0 )
	{
		return errno;
	}

	return 0;
}

int GE_ConnectClient(GE_NetworkSocket* cSocket, GE_NetworkSocket* nSocket)
{
	listen(nSocket->socketfd,1);

	socklen_t size = sizeof(cSocket->addr);
	cSocket->socketfd = accept(nSocket->socketfd,(struct sockaddr *)&cSocket->addr, &size); 
	if (nSocket->socketfd == -1 )
	{
		return -1;
	}
	return 0;

}
size_t GE_Read(GE_NetworkSocket* cSocket, char* buffer, size_t buffersize)
{
	size_t bytes_operated = read(cSocket->socketfd, buffer, buffersize);
	//buffer[buffersize] = '\0';
	return bytes_operated; 
}
size_t GE_ReadAll(GE_NetworkSocket* cSocket, char* buffer, size_t buffersize)
{
	size_t total_read = 0;
	auto temp_buffer = new char[buffersize];
	while(!(total_read >= buffersize)) //iterate till we've filled the buffer
	{
		size_t toRead = buffersize-total_read;

		size_t success_and_this_read_ammount = GE_Read(cSocket,temp_buffer,toRead);

		if (success_and_this_read_ammount == 0) //error - we expect to fill our buffer
		{
			delete[] temp_buffer;
			return 0;
		}

		//Copy the last-read string portion to its proper place in the final buffer.
		memcpy(buffer+total_read,temp_buffer,success_and_this_read_ammount);
		
		//Add the bytes that where read
		total_read += success_and_this_read_ammount;

	}
	delete[] temp_buffer;
	return total_read;
}
size_t GE_Write(GE_NetworkSocket* cSocket, const char* buffer, size_t buffersize)
{
	size_t bytes_operated = write(cSocket->socketfd,buffer,buffersize);
	//buffer[buffersize] = '\0';
	return bytes_operated;
}

void GE_FreeNetworkSocket(GE_NetworkSocket* socket)
{
	close(socket->socketfd);
	delete socket;
}



