#include "robot-control.h"

#ifdef SERVER


#include <wiringPi.h>

class wiringPiManager
{
	public:
		wiringPiManager()
		{
			wiringPiSetupPhys();
		}
} myWiringPiManager;

class wheel
{	
	public:
		wheel(int portForward, int portBackward)
		{
			this->portForward = portForward;
			this->portBackward = portBackward;
			pinMode(portForward, OUTPUT);
			pinMode(portBackward, OUTPUT);
		}
		void setSpeed(double speed)
		{
			this->speed = speed;
			if (speed == 0)
			{
				digitalWrite(portForward, LOW);
				digitalWrite(portBackward, LOW);
			}
			else if (speed > 0)
			{
				digitalWrite(portForward, HIGH);
				digitalWrite(portBackward, LOW);
			}
			else if (speed < 0)
			{
				digitalWrite(portForward, LOW);
				digitalWrite(portBackward, HIGH);
			}
		}
		double getSpeed()
		{
			return speed;
		}

	private:
		double speed = 0.f;
		int portForward;
		int portBackward;
};

/*!
 * start at 7, plug in fwd and leave 1 space then plug in reverse
 * after front's reverse, plug in back fwd and reverse in that order
 */
auto frontLeft = wheel(7,11);
auto backLeft = wheel(13,15);

/*!
 * start at 12, plug in fwd and leave 1 space then plug in reverse
 * leave one space and plug in back fwd then leave 1 space and plug in back back */
auto frontRight = wheel(12,16);
auto backRight = wheel(18,22);

int	Init_Robot()
{
}



void Shutdown_Robot()
{

}


void setLeft(double speed)
{
	frontLeft.setSpeed(speed);
	backLeft.setSpeed(speed);

}
void setRight(double speed)
{
	frontRight.setSpeed(speed);
	backRight.setSpeed(speed);
}

double getLeft()
{

}
double getRight()
{

}

#endif //SERVER
